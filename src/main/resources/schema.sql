CREATE DATABASE coderhouse;

CREATE TABLE cliente(
	cliente_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  	cliente_nombre VARCHAR(50),
    cliente_apellido VARCHAR(50),
  	cliente_dni INT (8)
);

CREATE TABLE producto(
  producto_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  producto_descripcion VARCHAR(150),
  producto_codigo VARCHAR(50),
  producto_stock INT,
  producto_precio DECIMAL
);

CREATE TABLE comprobante(
  comprobante_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  cliente_id INT,
  comprobante_fecha DATETIME,
  comprobante_cantidad INT,
  comprobante_total DECIMAL,
  CONSTRAINT FK_Cliente FOREIGN KEY (cliente_id)
  REFERENCES Cliente(cliente_id)
 );

CREATE TABLE venta (
    venta_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    venta_descripcion VARCHAR(150) NOT NULL,
    venta_cantidad INT,
    venta_precio DECIMAL,
    comprobante_id INT NOT NULL,
    producto_id INT NOT NULL,
    CONSTRAINT FK_Comprobante FOREIGN KEY (comprobante_id)
    REFERENCES Comprobante(comprobante_id),
    CONSTRAINT FK_Producto FOREIGN KEY (producto_id)
    REFERENCES Producto(producto_id)
);