package com.demo.FacturacionEntregaProyectoFinalDuran;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FacturacionEntregaProyectoFinalDuranApplication {

	public static void main(String[] args) {
		SpringApplication.run(FacturacionEntregaProyectoFinalDuranApplication.class, args);
	}

}
