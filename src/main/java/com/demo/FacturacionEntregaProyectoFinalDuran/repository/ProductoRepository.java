package com.demo.FacturacionEntregaProyectoFinalDuran.repository;

import com.demo.FacturacionEntregaProyectoFinalDuran.entity.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Integer> {
}
