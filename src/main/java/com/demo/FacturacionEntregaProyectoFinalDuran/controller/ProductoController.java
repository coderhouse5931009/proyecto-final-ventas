package com.demo.FacturacionEntregaProyectoFinalDuran.controller;

import com.demo.FacturacionEntregaProyectoFinalDuran.entity.Producto;
import com.demo.FacturacionEntregaProyectoFinalDuran.service.ProductoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @Operation(summary = "Get Products", description = "Permite obtener todos los productos.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operación exitosa"),
    })
    @GetMapping("/productos")
    public List<Producto> getProductos() {
        return productoService.GetProductos();
    }

    @Operation(summary = "Get Product by ID", description = "Permite obtener un producto por ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operación exitosa"),
            @ApiResponse(responseCode = "404", description = "Producto no encontrado")
    })
    @GetMapping("/productos/{id}")
    public ResponseEntity<Producto> getProductoById(@PathVariable Integer id) {
        Optional<Producto> foundProduct = productoService.getProductoById(id);
        return foundProduct.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Operation(summary = "Create Product", description = "Permite crear un nuevo producto.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operación exitosa")
    })
    @PostMapping("productos/nuevo")
    public ResponseEntity<Producto> add(@RequestBody Producto producto) {
        Producto createdProducto = productoService.createProduct(producto);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdProducto);
    }

    @Operation(summary = "Update Product by ID", description = "Permite actualizar un producto por ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operación exitosa"),
            @ApiResponse(responseCode = "404", description = "Producto no encontrado")
    })
    @PutMapping("/productos/actualizar/{id}")
    public ResponseEntity<Producto> update(@PathVariable Integer id, @RequestBody Producto producto) {
        Optional<Producto> updatedProducto = Optional.ofNullable(productoService.updateProducto(id, producto));
        return updatedProducto.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Operation(summary = "Delete Product by ID", description = "Permite eliminar un producto por ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operación exitosa"),
            @ApiResponse(responseCode = "404", description = "Producto no encontrado")
    })
    @DeleteMapping("productos/borrar/{id}")
    public ResponseEntity<Producto> delete(@PathVariable Integer id) {
        Optional<Producto> deletedProducto = Optional.ofNullable(productoService.deleteProducto(id));
        return deletedProducto.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
}