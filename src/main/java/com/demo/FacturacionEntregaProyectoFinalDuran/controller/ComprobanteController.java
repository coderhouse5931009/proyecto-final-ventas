package com.demo.FacturacionEntregaProyectoFinalDuran.controller;

import com.demo.FacturacionEntregaProyectoFinalDuran.entity.Comprobante;
import com.demo.FacturacionEntregaProyectoFinalDuran.repository.ClienteRepository;
import com.demo.FacturacionEntregaProyectoFinalDuran.repository.ProductoRepository;
import com.demo.FacturacionEntregaProyectoFinalDuran.service.ComprobanteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("")
public class ComprobanteController {

    @Autowired
    private ComprobanteService comprobanteService;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private ProductoRepository productoRepository;

    @Operation(summary = "Get Sales", description = "Permite obtener todos las ventas.")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Operación exitosa"),})
    @GetMapping("/comprobantes")
    public List<Comprobante> getComprobantes() {
        return comprobanteService.getComprobantes();
    }

    @Operation(summary = "Get Sale by ID", description = "Permite obtener una venta por ID.")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Operación exitosa"), @ApiResponse(responseCode = "404", description = "Venta no encontrada")})
    @GetMapping("/comprobantes/{id}")
    public ResponseEntity<Comprobante> getComprobanteById(@PathVariable Integer id) {
        Optional<Comprobante> foundComprobante = Optional.ofNullable(comprobanteService.getComprobanteById(id));
        return foundComprobante.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @Operation(summary = "Create Sale", description = "Permite crear una nueva venta.")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Operación exitosa")})
    @PostMapping("/comprobantes/nuevo")
    public ResponseEntity<Comprobante> addComprobante(@RequestBody Comprobante comprobante) {
        Comprobante createdComprobante = comprobanteService.save(comprobante);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdComprobante);
    }

}