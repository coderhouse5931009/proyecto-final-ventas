package com.demo.FacturacionEntregaProyectoFinalDuran.service;

import com.demo.FacturacionEntregaProyectoFinalDuran.entity.Cliente;
import com.demo.FacturacionEntregaProyectoFinalDuran.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    public List<Cliente> getClientes() {
        return this.clienteRepository.findAll();
    }

    public Optional<Cliente> getClienteById(Integer id) {
        return clienteRepository.findById(id);
    }

    public Cliente createCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente updateClient(Integer id, Cliente cliente) {
        Optional<Cliente> foundClient = clienteRepository.findById(id);
        if (foundClient.isEmpty()) {
            return null;
        } else {
            foundClient.get().setNombre(cliente.getNombre());
            foundClient.get().setApellido(cliente.getApellido());
            foundClient.get().setDni(cliente.getDni());
            return clienteRepository.save(foundClient.get());
        }
    }

    public Cliente deleteClient(Integer id) {
        Optional<Cliente> foundClient = clienteRepository.findById(id);
        if (foundClient.isPresent()) {
            clienteRepository.deleteById(id);
            return foundClient.get();
        } else{
            return null;
        }
    }

}
