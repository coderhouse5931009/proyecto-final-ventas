package com.demo.FacturacionEntregaProyectoFinalDuran.service;

import com.demo.FacturacionEntregaProyectoFinalDuran.entity.Producto;
import com.demo.FacturacionEntregaProyectoFinalDuran.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {
    @Autowired
    private ProductoRepository productoRepository;

    public List<Producto> GetProductos() {
        return this.productoRepository.findAll();
    }

    public Optional<Producto> getProductoById(Integer id) {
        return productoRepository.findById(id);
    }

    public List<Producto> getProductosByIds(List<Integer> ids) {
        return productoRepository.findAllById(ids);
    }
    public Producto createProduct(Producto producto) {
        return productoRepository.save(producto);
    }

    public Producto updateProducto(Integer id, Producto producto) {
        Optional<Producto> foundProduct = productoRepository.findById(id);
        if (foundProduct.isEmpty()) {
            return null;
        } else {
            foundProduct.get().setDescripcion(producto.getDescripcion());
            foundProduct.get().setPrecio(producto.getPrecio());
            foundProduct.get().setStock(producto.getStock());
            return productoRepository.save(foundProduct.get());
        }
    }

    public Producto deleteProducto(Integer id) {
        Optional<Producto> foundProduct = productoRepository.findById(id);
        if (foundProduct.isPresent()) {
            productoRepository.deleteById(id);
            return foundProduct.get();
        } else {
            return null;
        }
    }


}
