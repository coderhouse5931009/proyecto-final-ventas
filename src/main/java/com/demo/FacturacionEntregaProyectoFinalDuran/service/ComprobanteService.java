package com.demo.FacturacionEntregaProyectoFinalDuran.service;

import com.demo.FacturacionEntregaProyectoFinalDuran.entity.Cliente;
import com.demo.FacturacionEntregaProyectoFinalDuran.entity.Comprobante;
import com.demo.FacturacionEntregaProyectoFinalDuran.entity.Producto;
import com.demo.FacturacionEntregaProyectoFinalDuran.entity.Venta;
import com.demo.FacturacionEntregaProyectoFinalDuran.repository.ClienteRepository;
import com.demo.FacturacionEntregaProyectoFinalDuran.repository.ComprobanteRepository;
import com.demo.FacturacionEntregaProyectoFinalDuran.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class ComprobanteService {
    @Autowired
    private ComprobanteRepository comprobanteRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private ProductoRepository productoRepository;

    @Autowired
    private DateService dateService;

    public List<Comprobante> getComprobantes() {
        return crearComprobantes(this.comprobanteRepository.findAll());
    }

    public Comprobante save(Comprobante comprobante) {

        Boolean existeCliente = existeCliente(comprobante.getCliente());

        Boolean existenProductos = existenProductos(comprobante.getVentas());

        Boolean existeStock = existeStock(comprobante.getVentas());

        if (existeCliente && existenProductos && existeStock) {

            var comprobanteAGuardar = armarComprobante(comprobante);

            actualizarStock(comprobanteAGuardar.getVentas());

            return crearComprobante(this.comprobanteRepository.save(comprobanteAGuardar));
        } else {
            return new Comprobante();
        }
    }

    private void actualizarStock(Set<Venta> ventas) {
        for (Venta venta : ventas) {

            var cantidadVendida = venta.getCantidad();
            var producto = venta.getProducto();

            var productoDB = this.productoRepository.getById(producto.getProductoid());
            var stock = productoDB.getStock();
            var nuevoStock = stock - cantidadVendida;
            productoDB.setStock(nuevoStock);

            this.productoRepository.save(productoDB);

        }

    }

    public Comprobante getComprobanteById(Integer id) {

        var optionalCliente =  this.comprobanteRepository.findById(id);
        if (optionalCliente.isPresent()) {
            return crearComprobante(optionalCliente.get());
        } else {
            return new Comprobante();
        }
    }

    private List<Comprobante> crearComprobantes(List<Comprobante> comprobantes) {
        List<Comprobante> comprobantesDTOs = new ArrayList<Comprobante>();
        for (Comprobante comprobante : comprobantes) {
            comprobantesDTOs.add(this.crearComprobante(comprobante));
        }

        return comprobantesDTOs;
    }

    public Comprobante crearComprobante(Comprobante comprobante) {
        Comprobante dto = new Comprobante();

        dto.setComprobanteid(comprobante.getComprobanteid());

        dto.setCantidad(comprobante.getCantidad());

        dto.setFecha(comprobante.getFecha());

        dto.setTotal(comprobante.getTotal());

        dto.setCliente(comprobante.getCliente());

        dto.setVentas(crearVentas(comprobante.getVentas()));


        return dto;
    }

    private Set<Venta> crearVentas(Set<Venta> ventas) {
        Set<Venta> dtos = new HashSet<Venta>();

        for (Venta venta : ventas) {

            Venta dto = new Venta();

            dto.setId(venta.getId());

            dto.setCantidad(venta.getCantidad());

            dto.setDescripcion(venta.getDescripcion());

            dto.setPrecio(venta.getPrecio());

            dtos.add(dto);

        }

        return dtos;
    }

    private Comprobante armarComprobante(Comprobante comprobante) {
        var comprobanteAGuardar = new Comprobante();

        comprobanteAGuardar.setCliente(this.clienteRepository.findById(comprobante.getCliente().getId()).get());

        LocalDateTime now = dateService.obtenerFechaActual();
        comprobanteAGuardar.setFecha(now);


        comprobanteAGuardar.setVentas(new HashSet<Venta>());
        for (Venta venta : comprobante.getVentas()) {
            comprobanteAGuardar.addVenta(crearVenta(venta));
        }


        comprobanteAGuardar.setTotal(calcularTotal(comprobanteAGuardar.getVentas()));
        comprobanteAGuardar.setCantidad(comprobante.getVentas().size());

        return comprobanteAGuardar;
    }

    private BigDecimal calcularTotal(Set<Venta> ventas) {
        BigDecimal total = new BigDecimal("0");

        for (Venta venta : ventas) {
            total = total.add(new BigDecimal(venta.getPrecio().toString()));
        }

        return total;
    }

    private Venta crearVenta(Venta venta) {
        Venta ventaAGuardar = new Venta();

        Producto productoDB = this.productoRepository.findById(venta.getProducto().getProductoid()).get();
        ventaAGuardar.setCantidad(venta.getCantidad());
        ventaAGuardar.setDescripcion(productoDB.getDescripcion());
        ventaAGuardar.setPrecio(productoDB.getPrecio());
        ventaAGuardar.setProducto(productoDB);

        return ventaAGuardar;
    }

    private Boolean existeStock(Set<Venta> ventas) {
        for (Venta venta : ventas) {
            var productoid = venta.getProducto().getProductoid();
            var opProducto = this.productoRepository.findById(productoid);
            if (opProducto.isEmpty()) {
                return false;
            }
            if (venta.getCantidad() < opProducto.get().getStock()) {
                return true;
            }
        }
        return false;
    }

    private Boolean existenProductos(Set<Venta> ventas) {
        for (Venta venta : ventas) {
            var productoId = venta.getProducto().getProductoid();
            var opProducto = this.productoRepository.findById(productoId);
            if (opProducto.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private Boolean existeCliente(Cliente cliente) {
        var opCliente = this.clienteRepository.findById(cliente.getId());
        return !opCliente.isEmpty();
    }
}