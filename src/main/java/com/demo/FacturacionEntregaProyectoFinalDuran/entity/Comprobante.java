package com.demo.FacturacionEntregaProyectoFinalDuran.entity;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name="comprobante")
public class Comprobante {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "comprobante_id")
    private Integer comprobanteid;
    @Column(name = "comprobante_fecha")
    private LocalDateTime fecha;

    @Column(name = "comprobante_cantidad")
    private  Integer cantidad;
    @Column(name = "comprobante_total")
    private BigDecimal total;

    @ManyToOne
    @JoinColumn(name="cliente_id")
    private Cliente cliente;

    @OneToMany(mappedBy="comprobante", fetch=FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Venta> ventas;

    //Constructor
    public Comprobante() {

    }

    //Getters & Setters


    public Integer getComprobanteid() {
        return comprobanteid;
    }

    public void setComprobanteid(Integer comprobanteid) {
        this.comprobanteid = comprobanteid;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Set<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(Set<Venta> ventas) {
        this.ventas = ventas;
    }

    public Venta addVenta(Venta venta) {
        getVentas().add(venta);
        venta.setComprobante(this);

        return venta;
    }

    public Venta removeVenta(Venta venta) {
        getVentas().remove(venta);
        venta.setComprobante(null);

        return venta;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Comprobante [");
        if (comprobanteid != null)
            builder.append("comprobanteid=").append(comprobanteid);
        if (cantidad != null)
            builder.append("cantidad=").append(cantidad);
        if (fecha != null)
            builder.append("fecha=").append(fecha);
        if (total != null)
            builder.append("total=").append(total);
        if (cliente != null)
            builder.append("cliente=").append(cliente);
        if (ventas != null)
            builder.append("ventas=").append(ventas);
        builder.append("]");
        return builder.toString();
    }
}
