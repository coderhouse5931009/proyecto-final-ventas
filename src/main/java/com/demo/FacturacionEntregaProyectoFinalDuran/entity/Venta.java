package com.demo.FacturacionEntregaProyectoFinalDuran.entity;
import jakarta.persistence.*;

import java.math.BigDecimal;

@Entity
@Table(name="venta")
public class Venta {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)

    @Column(name = "venta_id")
    private Integer ventaid;

    @Column(name = "venta_cantidad")
    private Integer cantidad;

    @Column(name = "venta_descripcion")
    private String descripcion;

    @Column(name = "venta_precio")
    private BigDecimal precio;

    @ManyToOne
    @JoinColumn(name="comprobante_id")
    private Comprobante comprobante;

    @ManyToOne
    @JoinColumn(name="producto_id")
    private Producto producto;


    //Constructor
    public Venta() {
    }

    //Getters and Setters

    public Integer getId() {
        return ventaid;
    }

    public void setId(Integer ventaid) {
        this.ventaid = ventaid;
    }


    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Comprobante getComprobante() {
        return comprobante;
    }

    public void setComprobante(Comprobante comprobante) {
        this.comprobante = comprobante;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Venta [ventaid=").append(ventaid).append(", cantidad=").append(cantidad);
        if (descripcion != null)
            builder.append("descripcion=").append(descripcion);
        if (precio != null)
            builder.append("precio=").append(precio);
        builder.append("]");
        return builder.toString();
    }
}
