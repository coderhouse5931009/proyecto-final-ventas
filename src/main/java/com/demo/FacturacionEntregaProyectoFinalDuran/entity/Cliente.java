package com.demo.FacturacionEntregaProyectoFinalDuran.entity;

import jakarta.persistence.*;

@Entity
@Table(name="cliente")
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "cliente_id")
    private Integer id;
    @Column(name = "cliente_nombre")
    private String nombre;
    @Column(name = "cliente_apellido")
    private String apellido;
    @Column(name = "cliente_dni")
    private Integer dni;

    //Constructor
    public Cliente(){

    }

    //Getters & Setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }
}
