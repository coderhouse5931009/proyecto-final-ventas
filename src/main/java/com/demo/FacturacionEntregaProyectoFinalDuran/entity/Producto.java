package com.demo.FacturacionEntregaProyectoFinalDuran.entity;

import jakarta.persistence.*;

import java.math.BigDecimal;

@Entity
@Table(name="producto")
public class Producto {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)


    @Column(name = "producto_id")
    private Integer productoid;

    @Column(name = "producto_stock")
    private Integer stock;

    @Column(name = "producto_codigo")
    private String codigo;

    @Column(name = "producto_descripcion")
    private String descripcion;

    @Column(name = "producto_precio")
    private BigDecimal precio;

    //Constructor
    public Producto(){

    }

    //Getters y Setters

    public Integer getProductoid() {
        return productoid;
    }

    public void setProductoid(Integer productoid) {
        this.productoid = productoid;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Producto [");
        if (productoid != null)
            builder.append("productoid=").append(productoid).append(", ");
        if (stock != null)
            builder.append("stock=").append(stock).append(", ");
        if (codigo != null)
            builder.append("codigo=").append(codigo);
        builder.append("]");
        return builder.toString();
    }
}
